FROM nimlang/nim:latest-alpine

RUN apk add --no-cache cython python3 python3-dev linux-headers alpine-sdk build-base

# install the notebook package
RUN pip3 install --upgrade pip && \
    pip3 install pyzmq --install-option="--zmq=bundled" && \
    pip3 install notebook
